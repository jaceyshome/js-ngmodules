validateDOB = function(person) {
    var dobAttributes;
    dobAttributes = angular.copy(Validation.getModelAttributes('user', ['guardianDobDay', 'guardianDobMonth', 'guardianDobYear', 'guardianDob']));
    result = Validation.validateAttributes({
        values: person,
        attributes: dobAttributes
    });
    if (result === null) {
        return true;
    } else {
        return result.result; //result: message
    }
};

validSingleAttribute = function(person, key, forSubmit) {
    var result;
    result = Validation.validateAttributes({
        values: person,
        attributes: Validation.getModelAttributes('user', [key])
    });
    if (result === null) {
        return true;
    } else {
        return result.result;
    }
};