define(['angular'], function(angular) {
    var module;
    module = angular.module('common.validation.userattributes', []);
    return module.factory('UserAttributes', function() {
        return {
            male: {
                type: "boolean",
                required: true
            },
            name: {
                type: "string",
                required: true,
                maxLength: 105
            },
            firstName: {
                type: "string",
                required: true,
                maxLength: 50
            },
            lastName: {
                type: "string",
                required: true,
                maxLength: 55
            },
            dobDay: {
                type: "dd",
                required: true
            },
            dobMonth: {
                type: "mm",
                required: true
            },
            dobYear: {
                type: "yyyy",
                required: true
            },
            dob: {
                type: "date",
                aboveAge: 0,
                underAge: 19,
                required: true
            },
            school: {
                type: "string",
                required: true,
                maxLength: 255
            },
            teacher: {
                type: "string",
                required: false,
                maxLength: 105
            },
            address: {
                type: "address",
                required: true
            },
            street: {
                type: "string",
                required: true,
                maxLength: 255
            },
            suburb: {
                type: "string",
                required: true,
                maxLength: 46
            },
            state: {
                type: "select",
                "default": "state",
                required: true
            },
            postcode: {
                type: "number",
                required: true,
                maxLength: 9,
                minLength: 1
            },
            phone: {
                type: "number",
                required: false,
                maxLength: 10
            },
            guardian: {
                type: "string",
                required: true,
                maxLength: 30
            },
            guardianFirstName: {
                type: "string",
                required: true,
                maxLength: 50
            },
            guardianTitle: {
                type: "select",
                "default": "title",
                required: true
            },
            guardianLastName: {
                type: "string",
                required: true,
                maxLength: 55
            },
            email: {
                type: "string",
                email: true,
                required: true,
                minLength: 5,
                maxLength: 50
            },
            password: {
                type: "string",
                required: true,
                maxLength: 30
            },
            confirmPassword: {
                type: "string",
                required: true,
                maxLength: 30,
                match: "password"
            },
            guardianDobDay: {
                type: "dd",
                required: true
            },
            guardianDobMonth: {
                type: "mm",
                required: true
            },
            guardianDobYear: {
                type: "yyyy",
                required: true
            },
            guardianDob: {
                type: "date",
                required: true,
                aboveAge: 18
            },
            relation: {
                type: "select",
                "default": "relation",
                required: false
            }
        };
    });
});

//# sourceMappingURL=user.js.map
