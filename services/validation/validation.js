define(['angular', 'common/validation/user'], function(angular) {
    var module;
    module = angular.module('common.validation', ['common.validation.userattributes']);
    return module.factory('Validation', function(UserAttributes) {
        var calculateAge, checkAddress, checkBoolean, checkDD, checkDataValue, checkDate, checkDateString, checkEmail, checkMM, checkMatchingField, checkMaxLength, checkMinLength, checkNumber, checkRequired, checkSelect, checkType, checkYYYY, convertDateStringToDateArray, convertStringToBoolean, generateAttributes, generateKeyWords, service, validateAboveAge, validateDate, validateDateString, validateDay, validateMonth, validateUnderAge, validateYear, _attributeModels;
        _attributeModels = {
            user: UserAttributes
        };
        service = {};
        service.getModelAttributes = function(modelName, keys) {
            if (!(modelName || _attributeModels[modelName])) {
                return null;
            }
            if (keys && _attributeModels[modelName]) {
                return generateAttributes(modelName, keys);
            }
            return angular.copy(data[modelName]);
        };
        service.validateAttributes = function(data) {
            var key, result;
            result = null;
            for (key in data.attributes) {
                if (data.attributes.hasOwnProperty(key)) {
                    result = checkRequired(data, key);
                    if (result === 'o') {
                        if (data.values[key] === void 0) {
                            return null;
                        } else {
                            return checkDataValue(data, key);
                        }
                    } else if (result === null) {
                        result = checkDataValue(data, key);
                    } else {
                        return result;
                    }
                }
            }
            return result;
        };
        checkDataValue = function(data, key) {
            var result;
            result = null;
            result = checkType(data, key);
            if (result) {
                return result;
            } else {
                result = checkEmail(data, key);
                if (result) {
                    return result;
                } else {
                    result = checkMaxLength(data, key);
                    if (result) {
                        return result;
                    } else {
                        result = checkMinLength(data, key);
                        if (result) {
                            return result;
                        } else {
                            result = checkMatchingField(data, key);
                            if (result) {
                                return result;
                            } else {
                                result = checkAddress(data, key);
                                return result;
                            }
                        }
                    }
                }
            }
            return result;
        };
        validateDateString = function(date) {
            var data;
            if (!checkDateString(date)) {
                return false;
            }
            data = convertDateStringToDateArray(date);
            return validateYear(data[0]) && validateMonth(data[1]) && validateDate(data[0], data[1], data[2]);
        };
        convertDateStringToDateArray = function(date) {
            var data;
            data = date.split('-');
            if (data.length !== 3) {
                data = date.split('/');
                if (data.length !== 3) {
                    return false;
                }
            }
            return data;
        };
        checkDateString = function(date) {
            var pattern;
            pattern = new RegExp("[^-/0123456789]");
            if (!date.match(pattern)) {
                return true;
            } else {
                return false;
            }
        };
        validateYear = function(year) {
            if (!year) {
                return false;
            }
            if (year.length !== 4) {
                return false;
            }
            if (isNaN(year)) {
                return false;
            }
            return true;
        };
        validateMonth = function(month) {
            var tempMonth;
            if (!month) {
                return false;
            }
            if (month.length !== 1 && month.length !== 2) {
                return false;
            }
            tempMonth = parseInt(month);
            if (isNaN(tempMonth) || tempMonth < 1 || tempMonth > 12) {
                return false;
            }
            return true;
        };
        validateDay = function(day) {
            var tempDay;
            if (!day) {
                return false;
            }
            if (day.length !== 1 && day.length !== 2) {
                return false;
            }
            tempDay = parseInt(day);
            if (isNaN(tempDay) || tempDay < 1 || tempDay > 31) {
                return false;
            }
            return true;
        };
        validateDate = function(year, month, day) {
            var remainder, tempDay, tempMonth, tempYear;
            tempDay = parseInt(day);
            tempMonth = parseInt(month);
            tempYear = parseInt(year);
            if (isNaN(tempDay) || isNaN(tempMonth) || isNaN(tempYear)) {
                return false;
            }
            if (tempDay < 1 || tempDay > 31) {
                return false;
            }
            if (tempMonth === 4 || tempMonth === 6 || tempMonth === 9 || tempMonth === 11) {
                if (tempDay < 1 || tempDay > 30) {
                    return false;
                }
            }
            if (tempMonth === 2) {
                remainder = year % 4;
                if (remainder === 0) {
                    if (tempDay < 1 || tempDay > 29) {
                        return false;
                    }
                } else {
                    if (tempDay < 1 || tempDay > 28) {
                        return false;
                    }
                }
            }
            return true;
        };
        checkMaxLength = function(data, key) {
            var _ref;
            if (!((_ref = data.attributes[key]) != null ? _ref.maxLength : void 0)) {
                return null;
            }
            if (data.values[key].length > data.attributes[key].maxLength) {
                return {
                    key: key,
                    msg: generateKeyWords(key) + " max length is " + data.attributes[key].maxLength,
                    result: false
                };
            } else {
                return null;
            }
        };
        checkMinLength = function(data, key) {
            var _ref;
            if (!((_ref = data.attributes[key]) != null ? _ref.minLength : void 0)) {
                return null;
            }
            if (data.values[key].length < data.attributes[key].minLength) {
                return {
                    key: key,
                    msg: generateKeyWords(key) + " miniumn length is " + data.attributes[key].minLength,
                    result: false
                };
            } else {
                return null;
            }
        };
        checkEmail = function(data, key) {
            var re, _ref;
            if (!((_ref = data.attributes[key]) != null ? _ref.email : void 0)) {
                return null;
            }
            re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-z\-0-9]+\.)+[a-z]{2,}))$/;
            if (re.test(data.values[key])) {
                if (data.values[key].match(/[A-Z]/g)) {
                    return {
                        key: key,
                        msg: "Invalid email",
                        result: false
                    };
                }
                return null;
            }
            return {
                key: key,
                msg: "Invalid email",
                result: false
            };
        };
        checkRequired = function(data, key) {
            var _ref;
            if (((_ref = data.attributes[key]) != null ? _ref.required : void 0) !== true) {
                return 'o';
            }
            if (!data.values[key]) {
                return {
                    key: key,
                    msg: generateKeyWords(key) + " is required",
                    result: false
                };
            }
            return null;
        };
        checkType = function(data, key) {
            var result, _ref;
            if (!((_ref = data.attributes[key]) != null ? _ref.type : void 0)) {
                return null;
            }
            result = checkSelect(data, key);
            if (result) {
                return result;
            } else {
                result = checkNumber(data, key);
                if (result) {
                    return result;
                } else {
                    result = checkBoolean(data, key);
                    if (result) {
                        return result;
                    } else {
                        result = checkDate(data, key);
                        if (result) {
                            return result;
                        } else {
                            result = checkDD(data, key);
                            if (result) {
                                return result;
                            } else {
                                result = checkMM(data, key);
                                if (result) {
                                    return result;
                                } else {
                                    result = checkYYYY(data, key);
                                    return result;
                                }
                            }
                        }
                    }
                }
            }
            return null;
        };
        checkSelect = function(data, key) {
            var _ref, _ref1;
            if (((_ref = data.attributes[key]) != null ? _ref.type : void 0) !== 'select') {
                return null;
            }
            if (((_ref1 = data.attributes[key]) != null ? _ref1["default"] : void 0) == null) {
                return null;
            }
            if (!data.values[key] && data.attributes[key].required === true) {
                return {
                    key: key,
                    msg: generateKeyWords(key) + ' is not valid',
                    result: false
                };
            }
            if (data.values[key] === data.attributes[key]["default"]) {
                return {
                    key: key,
                    msg: generateKeyWords(key) + ' should not be same as default',
                    result: false
                };
            } else {
                return null;
            }
        };
        checkBoolean = function(data, key) {
            var _ref;
            if (((_ref = data.attributes[key]) != null ? _ref.type : void 0) !== 'boolean') {
                return null;
            }
            if (typeof convertStringToBoolean(data.values[key]) === 'boolean') {
                return null;
            }
            return {
                key: key,
                msg: generateKeyWords(key) + ' should be boolean true or false',
                result: false
            };
        };
        checkDate = function(data, key) {
            var result, _ref;
            if (((_ref = data.attributes[key]) != null ? _ref.type : void 0) !== 'date') {
                return null;
            }
            result = null;
            if (!validateDateString(data.values[key])) {
                return {
                    key: key,
                    msg: generateKeyWords(key) + " is not a valid date",
                    result: false
                };
            }
            result = validateUnderAge(data, key);
            if (result) {
                return result;
            }
            result = validateAboveAge(data, key);
            return result;
        };
        checkAddress = function(data, key) {
            return null;
        };
        validateUnderAge = function(data, key) {
            var result;
            if ((data.attributes[key] == null) || (data.attributes[key].underAge == null)) {
                return null;
            }
            result = calculateAge(data.values[key], data.attributes[key].underAge, false);
            if (result === true) {
                return null;
            } else {
                return {
                    key: key,
                    msg: generateKeyWords(key) + " must under " + data.attributes[key].underAge,
                    result: false
                };
            }
        };
        validateAboveAge = function(data, key) {
            var result;
            if ((data.attributes[key] == null) || (data.attributes[key].aboveAge == null)) {
                return null;
            }
            result = calculateAge(data.values[key], data.attributes[key].aboveAge, true);
            if (result === true) {
                return null;
            } else {
                return {
                    key: key,
                    msg: generateKeyWords(key) + " must above " + data.attributes[key].aboveAge,
                    result: false
                };
            }
        };
        calculateAge = function(date, inputAge, above) {
            var age, dateArray, day, m, month, today, year;
            if (above == null) {
                above = true;
            }
            dateArray = date.split("-");
            day = Number(dateArray[2]);
            month = Number(dateArray[1]);
            year = Number(dateArray[0]);
            today = new Date();
            age = today.getFullYear() - year;
            m = today.getMonth() + 1 - month;
            if (above === true) {
                if (age > inputAge) {
                    return true;
                } else {
                    if (age === inputAge) {
                        if (m > 0) {
                            return true;
                        } else {
                            if (m === 0) {
                                if (day < today.getDate()) {
                                    return true;
                                } else {
                                    return false;
                                }
                            } else {
                                return false;
                            }
                        }
                    } else {
                        return false;
                    }
                }
            } else {
                if (age < inputAge) {
                    return true;
                } else {
                    if (age === inputAge) {
                        if (m < 0) {
                            return true;
                        } else {
                            if (m === 0) {
                                if (day > today.getDate()) {
                                    return true;
                                } else {
                                    return false;
                                }
                            } else {
                                return false;
                            }
                        }
                    } else {
                        return false;
                    }
                }
            }
        };
        checkNumber = function(data, key, force) {
            var _ref;
            if (!force) {
                if (((_ref = data.attributes[key]) != null ? _ref.type : void 0) !== 'number') {
                    return null;
                }
            }
            if (!isNaN(data.values[key])) {
                return null;
            }
            return {
                key: key,
                msg: generateKeyWords(key) + ' should be number',
                result: false
            };
        };
        checkDD = function(data, key) {
            var result, _ref;
            if (((_ref = data.attributes[key]) != null ? _ref.type : void 0) !== 'dd') {
                return null;
            }
            data.attributes[key].maxLength = 2;
            result = checkNumber(data, key, true);
            if (result) {
                return result;
            } else {
                result = checkMaxLength(data, key);
                if (result) {
                    return result;
                }
            }
            if (!validateDay(data.values[key])) {
                return {
                    key: key,
                    msg: "<p>Please check the format of your Date of Birth<br/>DD/MM/YEAR</p>",
                    result: false
                };
            }
            return null;
        };
        checkMM = function(data, key) {
            var result, _ref;
            if (((_ref = data.attributes[key]) != null ? _ref.type : void 0) !== 'mm') {
                return null;
            }
            data.attributes[key].maxLength = 2;
            result = checkNumber(data, key, true);
            if (result) {
                return result;
            } else {
                result = checkMaxLength(data, key);
                if (result) {
                    return result;
                }
            }
            if (!validateMonth(data.values[key])) {
                return {
                    key: key,
                    msg: "<p>Please check the format of your Date of Birth<br/>DD/MM/YEAR</p>",
                    result: false
                };
            }
            return null;
        };
        checkYYYY = function(data, key) {
            var result, _ref;
            if (((_ref = data.attributes[key]) != null ? _ref.type : void 0) !== 'yyyy') {
                return null;
            }
            data.attributes[key].maxLength = 4;
            data.attributes[key].minLength = 4;
            result = checkNumber(data, key, true);
            if (result) {
                return result;
            } else {
                result = checkMaxLength(data, key);
                if (result) {
                    return result;
                } else {
                    result = checkMinLength(data, key);
                    if (result) {
                        return result;
                    }
                }
            }
            if (!validateYear(data.values[key])) {
                return {
                    key: key,
                    msg: "<p>Please check the format of your Date of Birth<br/>DD/MM/YEAR</p>",
                    result: false
                };
            }
            return null;
        };
        convertStringToBoolean = function(string) {
            switch (string.toLowerCase()) {
                case "true":
                case "yes":
                case "1":
                    return true;
                case "false":
                case "no":
                case "0":
                case null:
                    return false;
                default:
                    return Boolean(string);
            }
        };
        checkMatchingField = function(data, key) {
            var matchKey, _ref;
            if (!((_ref = data.attributes[key]) != null ? _ref.match : void 0)) {
                return null;
            }
            matchKey = data.attributes[key].match;
            if (data.values[matchKey] !== data.values[key]) {
                return {
                    key: key,
                    msg: generateKeyWords(key) + ' does not match ' + generateKeyWords(matchKey),
                    result: false
                };
            } else {
                return null;
            }
        };
        generateKeyWords = function(key) {
            var words;
            words = key.match(/[A-Z]?[a-z]+|[0-9]+/g);
            return words.join(" ").toLowerCase();
        };
        generateAttributes = function(modelName, keys) {
            var attributes, key, model, _i, _len;
            model = _attributeModels[modelName];
            attributes = {};
            for (_i = 0, _len = keys.length; _i < _len; _i++) {
                key = keys[_i];
                attributes[key] = model[key];
            }
            return attributes;
        };
        return service;
    });
});